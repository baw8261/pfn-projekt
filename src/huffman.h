/*
  Copyright (c) 2011 Dirk Willrodt <willrodt@zbh.uni-hamburg.de>
  Copyright (c) 2013 Stefan Kurtz <kurtz@zbh.uni-hamburg.de>
  Copyright (c) 2013 Center for Bioinformatics, University of Hamburg
*/

#ifndef HUFFMAN_H
#define HUFFMAN_H
#include <inttypes.h>
#include <stdio.h>

/* this type defines a struct which holds all necessary data to represent a
   huffman encoding of a text, represented by the distribution of its
   symbols. */
typedef struct Huffman_encoding Huffman_encoding;

/* gets an array of counts of length <numsymbols>. The i-th value in
   <distribution> is the number of occurrences of the i-th ASCII-Symbol in
   the given file. Return a representation of the Huffman-encoding.*/
Huffman_encoding *huffman_encoding_new(const unsigned long *distribution,
                                       unsigned long numsymbols);

/* the destructor */
void huffman_encoding_delete(Huffman_encoding *h_enc);

/* return size of encoding in bits */
uint64_t huffman_encoding_size(const Huffman_encoding *h_enc);

/* this is the type for the function to process a code of given length for
   the symbol with the given symbolnum and count-value. data refers
   to data which may be required for the processing */
typedef void (*Huffman_process_code)(unsigned int length,
                                     unsigned int code,
                                     unsigned int symbolnum,
                                     unsigned long count,
                                     void *data);

/* perform a traversal of the Huffman-tree and apply <process_code> to
   every node. <data> will be the last argument of any call to
   <process_code>. */
void huffman_encoding_traversal(const Huffman_encoding *h_enc,
                                Huffman_process_code process_code,
                                void *data);

/* perform decoding step: read encoding of file of length <totallength>
   from <infp> and decode the symbols using the given huffman encoding
   <h_enc>. If <symbolsinDNA> is 0, then the the decoded symbol is output.
   For encoding FastQ-sequences, symbolsinDNA defines the number of
   symbols in the DNA sequence. Output goes to standard out. */
void huffman_encoding_decode(FILE *infp,
                             uint64_t totallength,
                             unsigned long symbolsinDNA,
                             const Huffman_encoding *h_enc);

void huffman_encoding_fast_decode(FILE *infp,
                                  uint64_t totallength,
                                  int widthbits,
                                  const Huffman_encoding *h_enc);

typedef struct
{
  unsigned int length, symbolnum;
} Codespec;

Huffman_encoding *huffman_encoding_multi_insert(const Codespec *codespecs,
                                                const unsigned int *codetab,
                                                unsigned int numofsymbols);

/* return the number of internal nodes in the huffman tree */
unsigned long huffman_encoding_internalnodes(const Huffman_encoding *h_enc);

typedef struct Node Node;

#endif
