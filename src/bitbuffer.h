/*
  Copyright (c) 2013 Stefan Kurtz <kurtz@zbh.uni-hamburg.de>
  Copyright (c) 2013 Center for Bioinformatics, University of Hamburg

  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#ifndef BITBUFFER_H
#define BITBUFFER_H

#include <stdio.h>

/* The <GtBitbuffer> class provides methods to sequentially write
   bit-compressed integers to file as well as methods to read these
   from a file.*/
typedef struct GtBitbuffer GtBitbuffer;

/* Creates a new <GtBitbuffer> for output to <outfp>. */
GtBitbuffer* gt_bitbuffer_new(FILE *outfp);

/* Appends <value> of <bitsforvalue> bits to <bb>.
   <bitsforvalue> must not be larger than GT_BITSINBYTEBUFFER.  */
void gt_bitbuffer_next_value(GtBitbuffer *bb, unsigned long value,
                                     unsigned int bitsforvalue);

/* Append unary code of <value> to <bb>. The unary code consists of
   <value> 0's followed by a single 1. <value> can be larger than
   GT_BITSINBYTEBUFFER. */
void gt_bitbuffer_next_value_unary_code (GtBitbuffer *bb, unsigned long value);

/* Deletes <bb> and frees all associated memory. */
void gt_bitbuffer_delete(GtBitbuffer *bb);

/* Return number of bytes used for the bitbuffer */
unsigned int gt_bitbuffer_bytes(void);

/* Creates a new <GtBitbuffer> for input from <infp>. */
GtBitbuffer *gt_bitbuffer_new_get(FILE *infp);

/* Read the next <bitsforvalue> bits from <bb>. */
unsigned long gt_bitbuffer_next_value_get (GtBitbuffer *bb, 
                                           unsigned int bitsforvalue);

#endif
