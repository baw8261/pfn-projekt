#include <limits.h>
#include <stdlib.h>
#include <assert.h>
#include "codebook.h"

void add_entry(unsigned int length,
               unsigned int code,
               unsigned int symbolnum,
               unsigned long count,
               void *data) {
    Codebook_entry *codebook = (Codebook_entry *) data;
    codebook[symbolnum].count = count;
    codebook[symbolnum].code = code;
    codebook[symbolnum].length = length;
}

Codebook_entry *codebook_new(Huffman_encoding *h_enc) {

    Codebook_entry *codebook = calloc(UCHAR_MAX + 1,
                                      sizeof(*codebook));

    assert(codebook != NULL);
    huffman_encoding_traversal(h_enc, add_entry, codebook);

    return codebook;
}

void codebook_delete(Codebook_entry *codebook) {
    free(codebook);
}
