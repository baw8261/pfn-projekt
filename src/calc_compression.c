#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <ctype.h>
#include <assert.h>
#include <string.h>
#include <time.h>
#include "calc_compression.h"
#include "huffman.h"
#include "distribution.h"

unsigned long file_size(const char *filename) {
    long filesize;
    FILE *file = fopen(filename, "rb");

    assert (file != NULL);

    fseek(file, 0, SEEK_END);

    filesize = ftell(file);
    fclose(file);
    assert (filesize >= 0);
    return (unsigned long) filesize;
}

char *bitsequence_to_string(Bitsequence bs) {
    unsigned int idx;
    const unsigned int bits = CHAR_BIT * sizeof(Bitsequence);
    Bitsequence mask;
    char *buffer;

    buffer = malloc((bits + 1) * sizeof(*buffer));
    assert(buffer != NULL);
    for (idx = 0, mask = ((Bitsequence) 1) << (bits - 1);
         mask > 0; idx++, mask >>= 1) {
        buffer[idx] = (bs & mask) ? '1' : '0';
    }
    assert(idx == bits);
    buffer[idx] = '\0';
    return buffer;
}

void print_code(unsigned int length,
                unsigned int code,
                unsigned int symbolnum,
                unsigned long count,
                void *data) {
    data = NULL;

    if (count > 0) {
        if (isprint(symbolnum)) {
            printf("%c\t%lu\t%d\t", symbolnum, count, length);
        } else {
            printf("\\%d\t%lu\t%d\t", symbolnum, count, length);
        }
        char *buffer;

        buffer = bitsequence_to_string((Bitsequence) code);
        printf("%s\n", buffer + (strlen(buffer) - length));
        free(buffer);
    }
}

unsigned long get_bytes_for_bits(uint64_t bits) {
    if (bits % 8 == 0)
        return bits / 8;
    else
        return bits / 8 + 1;
}

double get_compression_ratio(uint64_t compressed_size,
                             unsigned long file_size) {
    if (file_size == 0)
        return 0;

    return (double) compressed_size / (double) file_size;
}

int main(int argc, char *argv[]) {
#undef TIME
#ifdef TIME
    clock_t begin, end;
    float runtime;
    begin = clock();
#endif
    if (argc != 2) {
        fprintf(stderr, "%s Invalid count of arguments\n", argv[0]);
        return EXIT_FAILURE;
    }
    unsigned long dist[UCHAR_MAX + 1] = {0};

    for (int i = 1; i < argc; i++) {
        distribution_update(dist, argv[0], argv[i]);
    }
    Huffman_encoding *h_enc;

    h_enc = huffman_encoding_new(dist, UCHAR_MAX + 1);
    printf("# symbol\tfreq\tcodelen\tcode\n");

    huffman_encoding_traversal(h_enc, print_code, NULL);

    unsigned long fileSize = file_size(argv[1]);
    uint64_t compressed_size = huffman_encoding_size(h_enc);
    unsigned long compressed_bytes = get_bytes_for_bits(compressed_size);
    double ratio = get_compression_ratio(compressed_size, fileSize);

    printf("# file\t%s\n", argv[1]);
    printf("# filesize (bytes)\t%lu\n", fileSize);
    printf("# compressed size (bits)\t%lu\n", compressed_size);
    printf("# compressed size (bytes)\t%lu\n", compressed_bytes);
    if (fileSize > 0)
        printf("# compression ratio (bits/byte)\t%.2f\n", ratio);

    huffman_encoding_delete(h_enc);

#ifdef TIME
    end = clock();
    runtime = end-begin;
    runtime /= CLOCKS_PER_SEC;
    printf("Laufzeit: %f Sekunden\n", runtime);
#endif
    exit(EXIT_SUCCESS);
}