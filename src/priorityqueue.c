/*
  Copyright (c) 2013 Stefan Kurtz <kurtz@zbh.uni-hamburg.de>
  Copyright (c) 2013 Center for Bioinformatics, University of Hamburg
*/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>
#include "priorityqueue.h"

#define HEAP_PARENT(X)  ((X) >> 1)
#define HEAP_LEFT(X)    ((X) << 1)

struct GtPriorityQueue
{
  GtCompare cmpfun;
  unsigned long capacity, numofelements;
  bool array_based;
  void **elements;
};

GtPriorityQueue *gt_priority_queue_new(GtCompare cmpfun,
                                       unsigned long maxnumofelements,
                                       unsigned long min_heap_size)
{
  GtPriorityQueue *pq = malloc(sizeof *pq);

  assert(pq != NULL);
  pq->elements = malloc(sizeof (*pq->elements) * (maxnumofelements + 1));
  assert(pq->elements != NULL);
  pq->cmpfun = cmpfun;
  pq->capacity = maxnumofelements;
  pq->array_based = maxnumofelements < min_heap_size ? true : false;
  pq->numofelements = 0;
  pq->elements[0] = NULL;
  return pq;
}

void gt_priority_queue_reset(GtPriorityQueue *pq)
{
  assert(pq != NULL);
  pq->numofelements = 0;
  pq->elements[0] = NULL;
}

bool gt_priority_queue_is_empty(const GtPriorityQueue *pq)
{
  assert(pq != NULL);
  return pq->numofelements == 0 ? true : false;
}

bool gt_priority_queue_is_full(const GtPriorityQueue *pq)
{
  assert(pq != NULL);
  return pq->numofelements == pq->capacity ? true : false;
}

unsigned long gt_priority_queue_size(const GtPriorityQueue *pq)
{
  return pq->numofelements;
}

void gt_priority_queue_add(GtPriorityQueue *pq, void *value)
{
  assert(pq != NULL && !gt_priority_queue_is_full(pq));
  if (pq->array_based)
  {
    void **ptr;

    /* store elements in reverse order, i.e.\ with the element of maximum
       priority at the last index */
    /* move elements to the right until an element larger or equal than
       the key is found. */
    for (ptr = pq->elements + pq->numofelements; ptr > pq->elements; ptr--)
    {
      if (pq->cmpfun(*(ptr-1),value) < 0)
      {
        *ptr = *(ptr-1);
      } else
      {
        break;
      }
    }
    *ptr = value;
    pq->numofelements++;
  } else
  {
    unsigned long idx = ++pq->numofelements;

    while (true)
    {
      const unsigned long parent = HEAP_PARENT(idx);

      if (parent == 0 || pq->cmpfun(pq->elements[parent],value) <= 0)
      {
        break;
      }
      assert(idx > 0);
      //printf("o: %lu <- %lu\n",idx,parent);
      pq->elements[idx] = pq->elements[parent];
      idx = parent;
    }
    assert(idx > 0);
    //printf("o: store elem at %lu\n",idx);
    pq->elements[idx] = value;
  }
}

void *gt_priority_queue_extract_max_prio(GtPriorityQueue *pq)
{
  void *max_prio_element;
  assert(pq != NULL && !gt_priority_queue_is_empty(pq));

  if (pq->array_based)
  {
    assert(pq->numofelements > 0);
    max_prio_element = pq->elements[--pq->numofelements];
  } else
  {
    unsigned long idx, child;
    void *lastelement;

    max_prio_element = pq->elements[1];
    lastelement = pq->elements[pq->numofelements--];
    for (idx = 1UL; HEAP_LEFT(idx) <= pq->numofelements; idx = child)
    {
      child = HEAP_LEFT(idx);
      assert(child > 0);
      if (child != pq->numofelements &&
          pq->cmpfun(pq->elements[child + 1],pq->elements[child]) < 0)
      {
        child++;
      }
      if (pq->cmpfun(lastelement,pq->elements[child]) > 0)
      {
        //printf("maxprio, o: %lu <= %lu\n",idx,child);
        pq->elements[idx] = pq->elements[child];
      } else
      {
        break;
      }
    }
    assert(idx > 0);
    //printf("maxprio, o: queue[%lu] <- lastelemen\n",idx);
    pq->elements[idx] = lastelement;
  }
  return max_prio_element;
}

const void *gt_priority_queue_find_max_prio(const GtPriorityQueue *pq)
{
  assert(pq != NULL && !gt_priority_queue_is_empty(pq));
  return pq->elements + (pq->array_based ? pq->numofelements-1
                                         : 1UL);
}

void gt_priority_queue_delete(GtPriorityQueue *pq)
{
  if (pq != NULL)
  {
    free(pq->elements);
    free(pq);
  }
}
