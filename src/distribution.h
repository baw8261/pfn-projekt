#ifndef MY_PFN_PROJEKT_2020_DISTRIBUTION_H
#define MY_PFN_PROJEKT_2020_DISTRIBUTION_H

unsigned long distribution_update(unsigned long *dist, const char *progname,
                                const char *filename);

/*void distribution_show(const unsigned long *dist);*/

#endif //MY_PFN_PROJEKT_2020_DISTRIBUTION_H