#ifndef MY_PFN_PROJEKT_2020_CALC_COMPRESSION_H
#define MY_PFN_PROJEKT_2020_CALC_COMPRESSION_H

#include <stdio.h>
#include <stdint.h>

unsigned long file_size(const char *filename);

typedef unsigned int Bitsequence;

char *bitsequence_to_string(Bitsequence bs);

void print_code(unsigned int length,
                unsigned int code,
                unsigned int symbolnum,
                unsigned long count,
                void *data);

unsigned long get_bytes_for_bits(uint64_t bits);

double get_compression_ratio(uint64_t compressed_size,
                             unsigned long file_size);

#endif //MY_PFN_PROJEKT_2020_CALC_COMPRESSION_H