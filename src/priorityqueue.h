/*
  Copyright (c) 2013 Stefan Kurtz <kurtz@zbh.uni-hamburg.de>
  Copyright (c) 2013 Center for Bioinformatics, University of Hamburg
*/

#ifndef PRIORITYQUEUE_H
#define PRIORITYQUEUE_H
#include <stdbool.h>

/*lst{Priorityqueuenterface}*/

/* The type of the compare function. It is like the compare function
   used in qsort */
typedef int (*GtCompare)(const void *,const void *);

/* The typename of the PriorityQueue */
typedef struct GtPriorityQueue GtPriorityQueue;

/* Create a new empty priority queue for at most maxnumofelements. Order
   the elements according to the comparison function cmpfun, which should
   return -1/1/0 if the first argument has less/more/the same priority
   as the second argument.
   There are two implementations of priority queues available:
   (1) One based on arrays with O(n) time for the add operation and O(1) time
       for the find_max_prio and extract_max_prio- operations, where n
       is the number of elements in the queue. This should be used if
       maxnumofelements is small.
   (2) One based on a heap with O(\log n) running time for the add,
       and the extract_max_prio-operations and O(1) time for the
       find_max_prio-operations.
   if maxnumofelements < min_heap_size, then the array based implementation
   is used, otherwise the heap based implemention
*/
GtPriorityQueue *gt_priority_queue_new(GtCompare cmpfun,
                                       unsigned long maxnumofelements,
                                       unsigned long min_heap_size);

/* reset the Queue, so that it becomes empty */
void gt_priority_queue_reset(GtPriorityQueue *pq);

/* Add element referred to by value to the queue. The queue only stores
   the reference and the user is responsible for allocating space for
   that element. */
void gt_priority_queue_add(GtPriorityQueue *pq, void *value);

/* return the number of elements stored in the queue */
unsigned long gt_priority_queue_size(const GtPriorityQueue *pq);

/* return reference to element in queue which has maximum priority. Delete
   the element from the queue. The space referred to can be reused. */
void *gt_priority_queue_extract_max_prio(GtPriorityQueue *pq);
/* return reference to element in queue which has maximum priority. Keep
   the reference in the queue. The space referred to cannot be reused. */
const void *gt_priority_queue_find_max_prio(const GtPriorityQueue *pq);
/* return true iff Queue is empty, i.e. contains no elements */
bool gt_priority_queue_is_empty(const GtPriorityQueue *pq);
/* return true iff Queue is full, i.e. has achieved its maximumum capacity
   as specified by the parameter maxnumofelements of the _new function.  */
bool gt_priority_queue_is_full(const GtPriorityQueue *pq);
/* Delete the space for the priority queue */
void gt_priority_queue_delete(GtPriorityQueue *pq);
/*lstend*/

#endif
