#!/bin/sh

set -e -x

# uncomment the following line if you want to debug your program
# and valgrind as well as valgrind.sh is available
#VALGRIND=valgrind.sh
for filename in `ls *.[cox]`
do
  ${VALGRIND} ./calc_compression.x ${filename}
done
filelist="../data/README ../data/a29.txt ../data/emptyfile ../data/shaks.data"
for filename in ${filelist} `ls ../data/Canterburycorpus/*`
do
  base=`basename ${filename}`
  ${VALGRIND} ./calc_compression.x ${filename} | diff - ../data/results_ccp/${base}.ccp
done
