#ifndef MY_PFN_PROJEKT_2020_CODEBOOK_H
#define MY_PFN_PROJEKT_2020_CODEBOOK_H

#include <inttypes.h>
#include <stdio.h>
#include "huffman.h"

typedef struct Codebook_entry Codebook_entry;

struct Codebook_entry {
    unsigned long count;
    unsigned int code;
    unsigned int length;
};

void add_entry(unsigned int length,
               unsigned int code,
               unsigned int symbolnum,
               unsigned long count,
               void *data);

Codebook_entry *codebook_new(Huffman_encoding *h_enc);

void codebook_delete(Codebook_entry *codebook);

#endif //MY_PFN_PROJEKT_2020_CODEBOOK_H
