#include <inttypes.h>
#include <assert.h>
#include <stdlib.h>
#include "bitbuffer.h"

#define GT_BITSINBYTEBUFFER 64U

struct GtBitbuffer {
    unsigned int remainingbitsinbuf; /* The number of bits which currently
                                      do not store any values */
    uint64_t currentbitbuffer,       /* this stores the bits of the buffer */
    numberofallvalues,               /* number of values already stored */
    readvalue;                       /* This is used for the bitbuffer in reading
                                      mode. It is used for reading the next
                                      64 bits/8 bytes from the input stream. */
    FILE *fp;                        /* Either the output stream or the input
                                      stream */
};

GtBitbuffer *gt_bitbuffer_new(FILE *outfp) {
    GtBitbuffer *bitbuffer = malloc(sizeof(*bitbuffer));
    assert(bitbuffer != NULL);
    bitbuffer->remainingbitsinbuf = GT_BITSINBYTEBUFFER;
    bitbuffer->currentbitbuffer = 0;
    bitbuffer->numberofallvalues = 0;
    bitbuffer->readvalue = 0;
    bitbuffer->fp = outfp;

    return bitbuffer;
}

void gt_bitbuffer_next_value(GtBitbuffer *bb, unsigned long value,
                             unsigned int bitsforvalue) {
    // Fall 1: Code passt genau in den Buffer
    if (bb->remainingbitsinbuf == bitsforvalue) {
        bb->currentbitbuffer |= value;
        fwrite(&bb->currentbitbuffer, sizeof(bb->currentbitbuffer), 1, bb->fp);
        bb->remainingbitsinbuf = GT_BITSINBYTEBUFFER;
        bb->currentbitbuffer = 0;
    }
        // Fall 2: Code passt in den Buffer, füllt ihn aber nicht komplett
    else if (bb->remainingbitsinbuf > bitsforvalue) {
        value = value << (bb->remainingbitsinbuf - bitsforvalue);
        bb->currentbitbuffer |= value;
        bb->remainingbitsinbuf -= bitsforvalue;
    }
        // Fall 3: Code passt nicht komplett in den Buffer
    else if (bb->remainingbitsinbuf < bitsforvalue) {
        unsigned long value_copy = value;
        unsigned int bitsforvalue_remain;

        value = value >> (bitsforvalue - bb->remainingbitsinbuf);
        bitsforvalue_remain = bitsforvalue - bb->remainingbitsinbuf;
        bb->currentbitbuffer |= value;
        fwrite(&bb->currentbitbuffer, sizeof(bb->currentbitbuffer), 1, bb->fp);
        bb->remainingbitsinbuf = GT_BITSINBYTEBUFFER;
        bb->currentbitbuffer = (value_copy
                << (bb->remainingbitsinbuf - bitsforvalue_remain));
        bb->remainingbitsinbuf -= bitsforvalue_remain;
    }
    bb->numberofallvalues++;
}

void gt_bitbuffer_delete(GtBitbuffer *bb) {
    fwrite(&bb->currentbitbuffer, sizeof(bb->currentbitbuffer), 1, bb->fp);
    free(bb);
}

GtBitbuffer *gt_bitbuffer_new_get(FILE *infp) {
    GtBitbuffer *bitbuffer = malloc(sizeof(*bitbuffer));
    assert(bitbuffer != NULL);
    bitbuffer->remainingbitsinbuf = 0;
    bitbuffer->currentbitbuffer = 0;
    bitbuffer->readvalue = 0;
    bitbuffer->numberofallvalues = 0;
    bitbuffer->fp = infp;

    return bitbuffer;
}

unsigned long gt_bitbuffer_next_bit(GtBitbuffer *bb) {
    if (bb->remainingbitsinbuf == 0) {
        if (fread(&bb->currentbitbuffer, sizeof(bb->currentbitbuffer), 1,
                  bb->fp) != 1) {

        }
        bb->remainingbitsinbuf = GT_BITSINBYTEBUFFER;
    }

    bb->readvalue = bb->currentbitbuffer >> (GT_BITSINBYTEBUFFER - 1);

    bb->currentbitbuffer = bb->currentbitbuffer << 1;
    bb->remainingbitsinbuf--;

    return bb->readvalue;
}

/* Read the next <bitsforvalue> bits from <bb>. */
unsigned long gt_bitbuffer_next_value_get(GtBitbuffer *bb,
                                          unsigned int bitsforvalue) {
    unsigned long value = 0;
    while (bitsforvalue > 0) {
        value = value << 1;
        value += gt_bitbuffer_next_bit(bb);
        bitsforvalue--;
    }
    return value;
}
