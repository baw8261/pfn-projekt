#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>
#include "distribution.h"

unsigned long distribution_update(unsigned long *dist, const char *progname,
                                const char *filename) {
    unsigned int currentCharacter;
    unsigned long filesize = 0;
    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        fprintf(stderr, "%s cannot open file (dist) %s \n", progname, filename);
        exit(EXIT_FAILURE);
    }
    while ((currentCharacter = fgetc(file)) != EOF) {
        dist[currentCharacter]++;
        filesize++;
    }
    fclose(file);

    return filesize;
}

/*void distribution_show(const unsigned long *dist)
{
    int i;

    for (i = 0; i <= UCHAR_MAX; i++)
    {
        if (dist[i] > 0)
        {
            if (iscntrl(i))
                printf("\\%d: %lu\n",i,dist[i]);
            else
                printf("%c: %lu\n",(char) i,dist[i]);
        }
    }
}*/

const unsigned long *binarysearch(const unsigned long *leftptr,
             const unsigned long *rightptr,
             unsigned long key)
{
    const unsigned long *midptr;
    while (leftptr <= rightptr)
    {
        midptr = leftptr + (unsigned long) (rightptr-leftptr)/2;
        if (key < *midptr)
        {
            rightptr = midptr - 1;
        } else
        {
            if (key > *midptr)
            {
                leftptr = midptr + 1;
            } else
                if (key == *midptr)
            {
                    while (*(midptr - 1) == key) {
                         midptr--;
                    }
                    return midptr;
            }
        }
    }
    if (key < *midptr)
        return midptr;
    return NULL;
}

int main (void) {

    const unsigned long array1[] = {10, 13, 15, 15, 17, 18, 20, 22, 25, 27};
    const unsigned long array2[] = {10, 13, 15, 15, 17, 18, 18, 18, 20, 22};
    const unsigned long array3[] = {20, 22, 25, 27};

    printf("Adresse: %lu\n", (unsigned long) &array2[5]);

    const unsigned long *element1 = binarysearch(&array1[0], &array1[9], 18);
    const unsigned long *element2 = binarysearch(&array2[0], &array2[9], 18);
    const unsigned long *element3 = binarysearch(&array3[0], &array3[3], 18);

    printf("element array 1: %lu\n", *element1);
    printf("element array 2: %lu\nAdresse: %lu\n", *element2, (unsigned long) element2);
    printf("element array 3: %lu\n", *element3);


    return EXIT_SUCCESS;
}


