#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include "distribution.h"
#include "huffman.h"
#include "codebook.h"
#include "bitbuffer.h"

typedef struct {
    bool decode;
    char *filename;
    char *progname;
} Options;

static void usage(const char *progname, bool showOptions) {
    fprintf(stderr, "Usage %s [options] <filename>\n", progname);
    if (showOptions) {
        fprintf(stderr,
                "Encode the given file.\n"
                "-d <filename.hf>\tDecode the given file.\n"
                "-h\tshow this usage message.\n");
    } else {
        fprintf(stderr, "Usage -h for more information.\n");
    }
}

static Options *option_new(int argc, char *const *argv) {
    bool error = false;

    if (argc < 2 || argc > 3) {
        error = true;
    }
    Options *options = malloc(sizeof(*options));
    options->progname = argv[0];
    options->filename = argv[1];
    options->decode = false;

    int opt;
    while ((opt = getopt(argc, argv, "dh?")) != -1) {
        switch ((char) opt) {
            default:
                assert((char) opt == '?');
                break;
            case 'd':
                options->decode = true;
                options->filename = argv[2];
                break;
            case 'h':
                usage(argv[0], true);
                free(options);
                return NULL;
                break;
        }
    }
    if (error) {
        free(options);
        usage(argv[0], false);
        return NULL;
    }
    return options;
}

char *create_filename(const char *file) {
    const char *suffix = ".hf";
    const size_t buffer_length = strlen(file) + strlen(suffix) + 1;
    char *buffer = malloc(buffer_length);
    int n = sprintf(buffer, "%s%s", file, suffix);

    assert(n <= buffer_length);
    return buffer;
}

bool encode(const char *progname, const char *file1) {
    bool error = false;
    unsigned long dist[UCHAR_MAX + 1] = {0};
    unsigned long fileSize = distribution_update(dist, progname, file1);
    Huffman_encoding *h_enc;

    h_enc = huffman_encoding_new(dist, UCHAR_MAX + 1);
    Codebook_entry *codebook = codebook_new(h_enc);

    huffman_encoding_delete(h_enc);

    // Output Datei erstellen
    char *filename = create_filename(file1);
    FILE *outfp = fopen(filename, "wb");
    if (outfp == NULL) {
        fprintf(stderr, "%s cannot create file %s \n", progname, filename);
        error = true;
    }

    GtBitbuffer *bitbuffer = gt_bitbuffer_new(outfp);

    gt_bitbuffer_next_value(bitbuffer, fileSize, 64);

    for (int i = 0; i < (UCHAR_MAX + 1); i++) {
        gt_bitbuffer_next_value(bitbuffer, dist[i], 64);
    }

    // Quelldatei nochmal einlesen
    FILE *infp = fopen(file1, "rb");
    if (infp == NULL) {
        fprintf(stderr, "%s cannot open file %s \n", progname, file1);
        error = true;
    }

    int currentCharacter;
    while ((currentCharacter = fgetc(infp)) != EOF) {
        gt_bitbuffer_next_value(bitbuffer, codebook[currentCharacter].code,
                                codebook[currentCharacter].length);
    }

    codebook_delete(codebook);
    gt_bitbuffer_delete(bitbuffer);
    free(filename);

    fclose(infp);
    fclose(outfp);
    return error;
}

bool decode(const char *progname, const char *file1) {
    bool error = false;
    FILE *file = fopen(file1, "rb");
    if (file == NULL) {
        fprintf(stderr, "%s cannot open file %s \n", progname, file1);
        error = true;
    }

    GtBitbuffer *bitbuffer = gt_bitbuffer_new_get(file);
    unsigned long filesize = gt_bitbuffer_next_value_get(bitbuffer, 64);
    unsigned long dist[UCHAR_MAX + 1] = {0};;

    for (int i = 0; i < (UCHAR_MAX + 1); i++) {
        dist[i] = gt_bitbuffer_next_value_get(bitbuffer, 64);
    }

    Huffman_encoding *h_enc = huffman_encoding_new(dist, UCHAR_MAX + 1);

    if (filesize > 0)
        huffman_encoding_decode(file, filesize, 0, h_enc);

    free(bitbuffer);
    huffman_encoding_delete(h_enc);

    fclose(file);
    return error;
}

int main(int argc, char *argv[]) {
    Options *options = option_new(argc, (char *const *) argv);
    bool error = false;

    if (options == NULL)
        return EXIT_FAILURE;

    if (options->decode == false)
        error = encode(options->progname, options->filename);
    else
        error = decode(options->progname, options->filename);

    free(options);
    return error ? EXIT_FAILURE : EXIT_SUCCESS;
}
