#!/bin/sh

set -e -x

# uncomment the following line if you want to debug your program
# and valgrind as well as valgrind.sh is available
VALGRIND=valgrind.sh
filelist="../data/README ../data/a29.txt ../data/emptyfile ../data/shaks.data"
for filename in ${filelist} `ls ../data/Canterburycorpus/*`
do
  ${VALGRIND} ./huffman.x ${filename} 
  ${VALGRIND} ./huffman.x -d ${filename}.hf | diff - ${filename}
  rm -f ${filename}.hf
done
