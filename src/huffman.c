#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "huffman.h"
#include "priorityqueue.h"
#include "bitbuffer.h"

struct Node {
    struct Node *left;
    struct Node *right;
    unsigned char character;
    unsigned long count;
};

struct Huffman_encoding {
    struct Node *root;
};

static int compareOccurrences(const void *node1, const void *node2) {
    const Node *a = (const Node *) node1;
    const Node *b = (const Node *) node2;

    if (a->count < b->count) {
        return -1;
    }
    if (a->count > b->count) {
        return 1;
    }
    if (a->character < b->character) {
        return -1;
    }
    if (a->character > b->character) {
        return 1;
    }

    return 0;
}

Huffman_encoding *huffman_encoding_new(const unsigned long *dist,
                                       unsigned long numsymbols) {
    GtPriorityQueue *queue = gt_priority_queue_new(compareOccurrences,
                                                   numsymbols, 50);
    for (unsigned long i = 0; i < numsymbols; i++) {
        if (dist[i] > 0) {
            Node *z = malloc(sizeof(*z));
            z->left = NULL;
            z->right = NULL;
            z->character = i;
            z->count = dist[i];
            gt_priority_queue_add(queue, z);
        }
    }
    while (gt_priority_queue_size(queue) >= 2) {
        Node *x = gt_priority_queue_extract_max_prio(queue);
        Node *y = gt_priority_queue_extract_max_prio(queue);
        Node *z = malloc(sizeof(*z));
        z->left = y;
        z->right = x;
        z->character = x->character < y->character ? y->character
                                                   : x->character;
        z->count = x->count + y->count;
        gt_priority_queue_add(queue, z);
    }

    Huffman_encoding *h_enc = malloc(sizeof(*h_enc));

    if (gt_priority_queue_is_empty(queue)) {
        h_enc->root = malloc(sizeof(*h_enc->root));
        h_enc->root->left = NULL;
        h_enc->root->right = NULL;
        h_enc->root->character = 0;
        h_enc->root->count = 0;
    } else {
        h_enc->root = gt_priority_queue_extract_max_prio(queue);
    }

    gt_priority_queue_delete(queue);

    return h_enc;
}

void huffman_node_traversal(unsigned int length,
                            unsigned int code,
                            Node *currentNode,
                            Huffman_process_code process_code,
                            void *data) {

    if (currentNode->left != NULL)
        huffman_node_traversal(length + 1,
                               (code << 1),
                               currentNode->left,
                               process_code,
                               data);

    if (currentNode->right != NULL)
        huffman_node_traversal(length + 1,
                               ((code << 1) | 1),
                               currentNode->right,
                               process_code,
                               data);

    if (currentNode->left == NULL && currentNode->right == NULL)
        process_code(length,
                     code,
                     currentNode->character,
                     currentNode->count,
                     data);
}

void huffman_encoding_traversal(const Huffman_encoding *h_enc,
                                Huffman_process_code process_code,
                                void *data) {
    unsigned int length = 0;
    unsigned int code = 0;

    huffman_node_traversal(length, code, h_enc->root, process_code, data);
}

unsigned int huffman_node_size(Node *currentNode, unsigned long length) {

    unsigned int size = 0;

    if (currentNode->left == NULL && currentNode->right == NULL) {
        return (currentNode->count * length);
    }

    if (currentNode->left != NULL)
        size += huffman_node_size(currentNode->left, length + 1);

    if (currentNode->right != NULL)
        size += huffman_node_size(currentNode->right, length + 1);

    return size;
}

uint64_t huffman_encoding_size(const Huffman_encoding *h_enc) {

    uint64_t encoding_size;
    unsigned long length = 0;

    encoding_size = huffman_node_size(h_enc->root, length);

    return encoding_size;
}

unsigned int huffman_node_count(Node *currentNode) {

    unsigned int count = 0;

    if (currentNode->left == NULL && currentNode->right == NULL) {
        return 1;
    }

    if (currentNode->left != NULL)
        count += huffman_node_count(currentNode->left);

    if (currentNode->right != NULL)
        count += huffman_node_count(currentNode->right);

    return count;
}

/* return the number of internal nodes in the huffman tree */
unsigned long huffman_encoding_internalnodes(const Huffman_encoding *h_enc) {
    return huffman_node_count(h_enc->root);
}

/* perform decoding step: read encoding of file of length <totallength>
   from <infp> and decode the symbols using the given huffman encoding
   <h_enc>. If <symbolsinDNA> is 0, then the the decoded symbol is output.
   For encoding FastQ-sequences, symbolsinDNA defines the number of
   symbols in the DNA sequence. Output goes to standard out. */
void huffman_encoding_decode(FILE *infp,
                             uint64_t totallength,
                             __attribute__((unused)) unsigned long symbolsinDNA,
                             const Huffman_encoding *h_enc) {

    GtBitbuffer *bitbuffer = gt_bitbuffer_new_get(infp);
    Node *currentNode = h_enc->root;
    unsigned long countnodes = huffman_encoding_internalnodes(h_enc);

    if (countnodes == 1) {
        assert(h_enc->root->left == NULL);
        for (unsigned long i = 0; i < totallength; i++) {
            printf("%c", (char) currentNode->character);
        }
    } else {
        unsigned long i = 0;
        while (i < totallength) {
            unsigned long currentBit = gt_bitbuffer_next_value_get(bitbuffer,
                                                                   1);

            if (currentBit == 1)
                currentNode = currentNode->right;
            else {
                assert(currentBit == 0);
                currentNode = currentNode->left;
            }

            if (currentNode->left == NULL && currentNode->right == NULL) {
                printf("%c", (char) currentNode->character);
                currentNode = h_enc->root;
                i++;
            }
        }
    }
    free(bitbuffer);
}

static void huffman_node_delete(Node *node) {
    if (node == NULL)
        return;
    if (node->left != NULL)
        huffman_node_delete(node->left);
    if (node->right != NULL)
        huffman_node_delete(node->right);
    free(node);
}

void huffman_encoding_delete(Huffman_encoding *h_enc) {
    assert(h_enc != NULL);
    huffman_node_delete(h_enc->root);
    free(h_enc);
}
